# Ansible Repository for deploying Services in an Automated Way

This Ansible Repository is a Deployment Framework for Services in a modern Way. It tries to be a Template for Testing Ansible Roles or Deploy Services with automation in mind.There is an CI/CD Pipeline included witch runs automaticaly in Gitlab , but if you have other ways it is very easy to adopt it to other deployment solutions.


## Feature Integrated CI/CD Pipeline 
Stages with Ansible tags for execution included:
- validate
- build
- preflight
- deploy-test
- deploy-canary
- deploy-production


#### Validate:
Validates the Deployment with `ansible-playbook -i inventory/all.yml all-services.yml --syntax-check -vvvv`

#### Build:
Builds the configuration files from the Deployment with `ansible-playbook -i inventory/all.yml all-services.yml --tags build --vault-password-file /tmp/vault-password`

#### Inventory:
Some words about the Inventory

#### Environments:
Some words about the Environments that are tested


## Feature Artifacts

Artifacts are collected on all stages . On the different stages there may be different artifacts like config files on the build stage and some reports on the deployment stage.

Artifacts path:

| path | description | Ansible tag|
|--- | --- |--- | 
| build | some artifacts from the build process (configs/packages)| [build]|
| test | Files or Reports from the Testing stage | [build] |
| state | some artifacts from the deployment process (diffs/reports) | [deploy] |
| docs | some artifacts from the generated documentation | [build,docs]|

 
License:
    MIT / BSD

Author Information:
roland@stumpner.at
