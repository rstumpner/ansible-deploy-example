# .gitlab-ci.yml
# List all Stages of the CI/CD Pipeline
stages:
  - validation
  - build
  - preflight
  - deploy-test
  - package
  - prepare-canary
  - deploy-canary
  - prepare-production
  - deploy-production


# Set the default Variables for the deployment container
variables:
  ANSIBLE_FORCE_COLOR: 'true'
  GIT_SSL_NO_VERIFY: "1"
  ANSIBLE_HOST_KEY_CHECKING: 'false'

# Set the default image for the deployment container
image: ubuntu:18.04

# Setting the default Artifacts directorys


# Set the default before_script for the ansible installation
before_script:
  - apt-get update -qq
  - apt install --no-install-recommends -y python-minimal software-properties-common git vim curl iputils-ping mtr dnsutils rsync tree python-pip
  - apt-add-repository ppa:ansible/ansible
  - apt update
  - apt install -y ansible python-markupsafe python-ecdsa libyaml-0-2 python-jinja2 python-yaml python-paramiko python-httplib2 python-crypto sshpass
  - ansible --version
  - echo $ANSIBLE_VAULT_KEY > /tmp/vault-password
  - ansible-galaxy install -r roles/requirements.yml

# Stage Validation: Check the Ansible Playbook Construct and some Variables
# Complete Funktional Test Ansible --syntax-check
validation-syntax:
  stage: validation
  script:
    - 'ansible-playbook -i inventory/all.yml all-services.yml --syntax-check -vvvv'

validation-tasks:
  stage: validation
  script:
    - 'ansible-playbook -i inventory/all.yml all-services.yml --list-tasks'

validation-inventory:
  stage: validation
  script:
     - ansible-inventory -i inventory/all.yml --list

# Stage Build: Build all Config Files and Basics for the Status Site
build:
  stage: build
  artifacts:
    when: always
    paths:
      - tests/
      - build/
      - state/
      - docs/
  variables:
    CD_ENVIRONMENT: test
  script:
    - 'ansible-playbook -i inventory/all.yml all-services.yml --tags build --limit $CD_ENVIRONMENT --vault-password-file /tmp/vault-password'

# Stage Preflight: Test and Prechecks before Deploying
# preflight check with ansible --diff --check Mode
preflight-test:
  stage: preflight
  allow_failure: true
  artifacts:
    when: always
    paths:
      - tests/
      - build/
      - state/
      - docs/
  variables:
    CD_ENVIRONMENT: test
  script:
    - 'ansible-playbook -i inventory/all.yml all-services.yml --skip-tags build --diff --check --limit $CD_ENVIRONMENT --vault-password-file /tmp/vault-password'

# Stage Deploy Test: Deploy the Test Environment

deploy-test:
  stage: deploy-test
  environment:
    name: test
  variables:
    CD_ENVIRONMENT: test
  artifacts:
    when: always
    paths:
      - tests/
      - build/
      - state/
      - docs/
  script:
    - 'ansible-playbook -i inventory/all.yml all-services.yml --skip-tags build --limit $CD_ENVIRONMENT --vault-password-file /tmp/vault-password'

package-configs:
  stage: package
  variables:
    PACKAGENAME: '${CI_PROJECT_NAME}-config'
    VERSION: '1.0.0'
#   VERSION: ${CI_COMMIT_TAG}
  image: curlimages/curl:latest
  before_script:
     - echo "copy all build files from roles to a central repository"
  script:
    - tar -czvf ${PACKAGENAME}.${VERSION}.tar.gz roles/
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${PACKAGENAME}.${VERSION}.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGENAME}/${VERSION}/${PACKAGENAME}.${VERSION}.tar.gz"'

package-documentation:
  stage: package
  variables:
    PACKAGENAME: '${CI_PROJECT_NAME}-docs'
    VERSION: '1.0.0'
#   VERSION: ${CI_COMMIT_TAG}
#  image: curlimages/curl:latest
#  before_script:
#     - echo "Grab all docs and copy to package directory"
#     - ansible-playbook -i inventory/all.yml playbooks/notfall-template.yml
  script:
    - echo "Grab all docs and copy to package directory"
    - ansible-playbook -i inventory/all.yml playbooks/stage-package.yml
    - tar -czvf ${PACKAGENAME}.${VERSION}.tar.gz docs/
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${PACKAGENAME}.${VERSION}.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGENAME}/${VERSION}/${PACKAGENAME}.${VERSION}.tar.gz"'


prepare-canary:
  stage: prepare-canary
  only:
    - master
  environment:
    name: canary
  artifacts:
    when: always
    paths:
      - tests/
      - build/
      - state/
      - docs/
  variables:
    CD_ENVIRONMENT: canary
  script:
    - 'ansible-playbook -i inventory/all.yml all-services.yml --skip-tags build --tags prepare-canary --limit $CD_ENVIRONMENT --vault-password-file /tmp/vault-password'


# Stage Deploy Canary: Deploy the Canary Environment

deploy-canary:
  stage: deploy-canary
  when: manual
  only:
    - master
  environment:
    name: canary
  artifacts:
    when: always
    paths:
      - tests/
      - build/
      - state/
      - docs/
  variables:
    CD_ENVIRONMENT: canary
  script:
    - 'ansible-playbook -i inventory/all.yml all-services.yml --skip-tags build --limit $CD_ENVIRONMENT --vault-password-file /tmp/vault-password'


prepare-production:
  stage: prepare-production
  only:
    - tags
  environment:
    name: production
  artifacts:
    when: always
    paths:
      - tests/
      - build/
      - state/
      - docs/
  variables:
    CD_ENVIRONMENT: production
  script:
    - 'ansible-playbook -i inventory/all.yml all-services.yml --skip-tags build --tags prepare-production --limit $CD_ENVIRONMENT --vault-password-file /tmp/vault-password'



# Stage Deploy Production: Deploy the Production Environment

deploy-production:
  stage: deploy-production
  only:
    - tags
  when: manual
  environment:
    name: production
  artifacts:
    when: always
    paths:
      - tests/
      - build/
      - state/
      - docs/
  variables:
    CD_ENVIRONMENT: production
  script:
    - 'ansible-playbook -i inventory/all.yml all-services.yml --skip-tags build --limit $CD_ENVIRONMENT --vault-password-file /tmp/vault-password'
